#!/bin/bash
#
# Resize/recanvas images. Then creates individual clips and combines them all into one video.
#

# make sure there is a slides directory
if [ ! -d "slides" ]; then
  mkdir slides;
  echo 'A directory named `slides` has been created for you. Please place your slides in this directory.';
  exit;
fi

if [ "" = "$(dpkg-query -W --showformat='${Status}\n' ffmpeg | grep 'install ok installed')" ]; then
	echo;
	echo 'ffmpeg is required to run this script. Please install ffmpeg and try again.';
	echo;
	exit;
fi

# create the directories and files that will be used
mkdir -p ./video_clips;
mkdir -p ./tmp;

# cleanup directories so there are no lingering files
rm -rf ./tmp/*;



#############################################################
# loop through all JPGs and build each one individually
#############################################################
echo;
echo "If you have already run the script and no slides have been modified you do not need to generate the slides again.";
echo;
read -p "Would you like to generate clips? [Y/n] " generate_clips;
echo;

if [[ $generate_clips =~ ^[Yy]$ || $generate_clips == '' ]]; then

    # remove existing clips
    echo 'Deleting any existing clip files...';
    rm -rf ./video_clips/*;

	for jpg in $(ls slides/*.jpg); do

		jpg_basename=$(basename $jpg);
    
		# ImageMagick resize/recanvas and auto rotate/orient
		convert $jpg -auto-orient -resize 1280x720 -background black -gravity center -extent 1280x720 -quality 100 tmp/$jpg_basename;

		# FFMPEG creates the individual clip
		ffmpeg -y -framerate 1/6 -i tmp/$jpg_basename -c:v libx264 -vf "fps=30,fade=in:0:15:color=black,fade=out:165:15:color=black,format=yuv420p" video_clips/$(basename $jpg_basename .jpg).mp4;

		# remove the resize/recanvased JPG
		rm tmp/$jpg_basename;

	done;

echo "#######################################################";
echo "Clips of each image have been generated.";
echo "#######################################################";

fi

#############################################################
# Take all of the individual clips and combine
#############################################################

# write a list of videos in random order
echo;
echo "Now a text file will be generated with the list of clips to generate the slideshow. The order of the";
echo "list in this text file will determine the order of clips in the slideshow.";
echo;
echo 'Choose:';
echo 'Y for yes';
echo 'n for no';
echo 's to skip (e.g. if one already exists)';
echo;
read -p "Randomize the order of the slides? [Y/n/s] " randomize;
echo;

# us the user input to determine if we are randomizing the slides
if [[ $randomize =~ ^[yY]$ || $randomize == '' ]]; then
	echo '' > clips_list.txt; # create an empty clip file
	find ./video_clips/ -type f -printf "file video_clips/%f\n" | shuf -o clips_list.txt;
	echo "A random clips list has been generated.";
elif [[ $randomize =~ ^[sS]$ ]]; then
	echo "No clips file was generated.";
else
	for v in $(ls video_clips/*); do
		echo '' > clips_list.txt; # create an empty clip file
		echo "file $v" >> clips_list.txt;
		echo 'A clip list, ordered by filename, has been generated.';
	done;
fi

echo;
echo 'If you would like to make any changes edit the order in clips_list.txt.';
echo;
read -p "Press [ENTER] when you are ready to continue. " enter;
echo;
echo "#######################################################";
echo "#######################################################";
echo "CREATING THE FINAL VIDEO";
echo "#######################################################";
echo "#######################################################";
echo;

# Remove the slideshow file if one already exists.
if [ -f "slideshow.mp4" ]; then
	echo "Slideshow file found. Deleting the file before generating the new one."
	rm slideshow.mp4;
fi

# Use the above list to generate the final video.
ffmpeg -y -f concat -i clips_list.txt -c copy slideshow.mp4

# Do cleanup.
rmdir ./tmp