This is a simple Bash script that takes all of the images in the `slides` directory and combines them into a single video (MP4) in the form of a slideshow. Each image fades in and out.

# Requirements

* A *nix environment to to run this bash script.
* `ffmpeg` installed.

# How to use it
* Place all of your images in the `slides` directory. Be sure all images are named `*.jpg` (not .jpeg) and there are no spaces in the names.
* Make `generate.sh` executable.
* Run `generate.sh`. (optional: When prompted, adjust the order of the slides by editing the order in `clips_list.txt`.)
* Done! A new file is generated called `slideshow.mp4`.

